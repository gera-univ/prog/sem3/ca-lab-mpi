#include <iostream>
#include <mpi.h>
#include <random>
#include <functional>
#include <iomanip>
#include "common.h"

std::vector<float> getRandomMatrix(int rows, int cols, float from, float to);

void printMatrix(const std::vector<float> &matrix, int cols);

int main(int argc, char *argv[]) {
    MPI_Init(nullptr, nullptr);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    std::vector<float> matrix1;
    std::vector<float> matrix2;

    int rows, cols;
    if (argc != 3)
        MPI_Abort(MPI_COMM_WORLD, 1);
    std::istringstream arg1(argv[1]);
    arg1 >> rows;
    std::istringstream arg2(argv[2]);
    arg2 >> cols;


    if (world_rank == 0) {

        matrix1 = getRandomMatrix(rows, cols, -1000, 1000);
        matrix2 = getRandomMatrix(rows, cols, -1000, 1000);

        std::cout << std::showpos << std::fixed << std::setprecision(4);
        std::cout << "matrix1\n";
        printMatrix(matrix1, cols);
        std::cout << "matrix2\n";
        printMatrix(matrix2, cols);
    }

    std::vector<float> subMatrix1;
    std::vector<float> subMatrix2;
    auto[counts, displs] = scatterVector(matrix1, rows * cols, subMatrix1, MPI_FLOAT, world_size, world_rank);
    scatterVector(matrix2, rows * cols, subMatrix2, MPI_FLOAT, world_size, world_rank);

    int subMatrixSize;
    if (world_rank == 0) {
        for (int i = 0; i < world_size; ++i) {
            MPI_Send(counts.data() + i, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
    }

    MPI_Recv(&subMatrixSize, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);


    std::vector<float> resultSubMatrix(subMatrixSize);

    if (world_rank == 1) {
        std::cout << "submatrix1\n";
        printMatrix(subMatrix1, cols);
        std::cout << "submatrix2\n";
        printMatrix(subMatrix2, cols);
    }

    std::transform(subMatrix1.begin(), subMatrix1.end(), subMatrix2.begin(), resultSubMatrix.begin(), std::plus<>());

    std::vector<float> resultMatrix;

    if (world_rank == 0) {
        resultMatrix.resize(rows * cols);
    }

    MPI_Gather(resultSubMatrix.data(), subMatrixSize, MPI_FLOAT, resultMatrix.data(), subMatrixSize, MPI_FLOAT, 0,
               MPI_COMM_WORLD);

    if (world_rank == 0) {
        printVector(displs, "displs");
        printVector(counts, "counts");
        std::cout << "the sum of two matrices is \n";
        printMatrix(resultMatrix, cols);
    }

    MPI_Finalize();
    return 0;
}

void printMatrix(const std::vector<float> &matrix, int cols) {
    for (int i = 0; i < matrix.size(); ++i) {
        std::cout << matrix[i] << "\t\t";
        if ((i + 1) % cols == 0)
            std::cout << "\n";
    }
    std::cout << "\n";
}

std::vector<float> getRandomMatrix(int rows, int cols, float from, float to) {
    std::vector<float> matrix(rows * cols);

    std::random_device rd;
    std::mt19937 re(clock());
    std::uniform_real_distribution ud(from, to);
    auto genNumber = [&ud, &re] { return ud(re); };

    std::generate(matrix.begin(), matrix.end(), genNumber);

    return matrix;
}
