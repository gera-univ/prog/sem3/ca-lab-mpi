#include <iostream>
#include <mpi.h>
#include <random>
#include <functional>
#include <iomanip>
#include "common.h"

std::vector<float> getRandomMatrix(int rows, int cols, float from, float to);

void printMatrix(const std::vector<float> &matrix, int cols);

int main(int argc, char *argv[]) {
    MPI_Init(nullptr, nullptr);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    int matrix1Rows, matrix1Columns, matrix2Columns;
    if (argc != 4)
        MPI_Abort(MPI_COMM_WORLD, 1);
    std::istringstream arg1(argv[1]);
    arg1 >> matrix1Rows;
    std::istringstream arg2(argv[2]);
    arg2 >> matrix1Columns;
    std::istringstream arg3(argv[3]);
    arg3 >> matrix2Columns;

    std::vector<float> matrix1;
    std::vector<float> matrix2(matrix1Columns * matrix2Columns);

    if (world_rank == 0) {

        matrix1 = getRandomMatrix(matrix1Rows, matrix1Columns, -1, 1);
        matrix2 = getRandomMatrix(matrix1Columns, matrix2Columns, -1, 1);

        std::cout << std::showpos << std::fixed << std::setprecision(2);
        std::cout << "matrix1\n";
        printMatrix(matrix1, matrix1Columns);
        std::cout << "matrix2\n";
        printMatrix(matrix2, matrix2Columns);
    }

    int subMatrixRowsMax = (matrix1Rows + world_size - 1) / world_size;
    std::vector<int> subMatrixRows(world_size);
    std::vector<int> sendCounts(world_size);
    std::vector<int> displacements(world_size);
    int srcSize = matrix1Rows;
    for (int i = 0, n = 0; i < displacements.size(); ++i, n += subMatrixRowsMax, srcSize -= subMatrixRowsMax) {
        displacements[i] = n * matrix1Columns;
        if (srcSize > 0) {
            if (srcSize - subMatrixRowsMax < 0)
                subMatrixRows[i] = srcSize;
            else
                subMatrixRows[i] = subMatrixRowsMax;
        } else subMatrixRows[i] = 0;

        sendCounts[i] = subMatrixRows[i] * matrix1Columns;
    }
    std::vector<float> subMatrix(subMatrixRows[world_rank] * matrix1Columns);
    std::vector<float> resultSubMatrix(subMatrixRows[world_rank] * matrix2Columns);

    if (world_rank == 0) {
        std::cout << "matrix1Columns: " << matrix1Columns << "\n";
        printVector(sendCounts, "sendCounts");
        printVector(displacements, "displacements");
    }

    MPI_Bcast(matrix2.data(), matrix2.size(), MPI_FLOAT, 0, MPI_COMM_WORLD);
    MPI_Scatterv(matrix1.data(), sendCounts.data(), displacements.data(), MPI_FLOAT, subMatrix.data(),
                 subMatrix.size(), MPI_FLOAT, 0, MPI_COMM_WORLD);

    for (int k = 0; k < subMatrixRows[world_rank]; ++k) {
        for (int j = 0; j < matrix2Columns; ++j) {
            double dotProduct = 0;
            for (int i = 0; i < matrix1Columns; ++i) {
                dotProduct += subMatrix[k * matrix1Columns + i] *
                              matrix2[(j + i * matrix2Columns) % matrix2.size()];
            }
            resultSubMatrix[k * matrix2Columns + j] = dotProduct;
        }
    }

    std::vector<float> resultMatrix;
    if (world_rank == 0) {
        resultMatrix.resize(matrix2Columns * matrix1Rows);
        std::cout << "result matrix size: " << resultMatrix.size() << "\n";
    }

    std::vector<int> resultSubMatrixSendCounts(world_size);
    std::vector<int> resultSubMatrixDispls(world_size);
    for (int i = 0; i < world_size; ++i) {
        int delta = subMatrixRows[i] * matrix2Columns;
        resultSubMatrixSendCounts[i] = delta;
        if (i < world_size - 1)
            resultSubMatrixDispls[i + 1] = resultSubMatrixDispls[i] + delta;
    }

    if (world_rank == 0) {
        printVector(resultSubMatrixSendCounts, "resultSubMatrixSendCounts");
        printVector(resultSubMatrixDispls, "resultSubMatrixDispls");
    }
    MPI_Gatherv(resultSubMatrix.data(), resultSubMatrix.size(), MPI_FLOAT, resultMatrix.data(),
                resultSubMatrixSendCounts.data(), resultSubMatrixDispls.data(), MPI_FLOAT, 0, MPI_COMM_WORLD);

    if (world_rank == 0) {
        std::cout << "\nthe product of two matrices is \n";
        printMatrix(resultMatrix, matrix2Columns);
    }

    MPI_Finalize();
    return 0;
}

void printMatrix(const std::vector<float> &matrix, int cols) {
    for (int i = 0; i < matrix.size(); ++i) {
        std::cout << matrix[i] << " ";
        if ((i + 1) % cols == 0)
            std::cout << "\n";
    }
    std::cout << "\n";
}

std::vector<float> getRandomMatrix(int rows, int cols, float from, float to) {
    std::vector<float> matrix(rows * cols);

    std::random_device rd;
    std::mt19937 re(clock());
    std::uniform_real_distribution ud(from, to);
    auto genNumber = [&ud, &re] { return ud(re); };

    std::generate(matrix.begin(), matrix.end(), genNumber);

    return matrix;
}

