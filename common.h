#pragma once

#include <mpi.h>
#include <vector>
#include <string>

template <typename T>
std::pair<std::vector<int>, std::vector<int>>
scatterVector(const std::vector<T> &src, int srcSize, std::vector<T> &dest, MPI_Datatype datatype,
              int world_size, int world_rank) {
    int subVectorSizeMax = (srcSize + world_size - 1) / world_size;

    std::vector<int> sendCounts(world_size, subVectorSizeMax);
    sendCounts[world_size - 1] = srcSize - subVectorSizeMax * (world_size - 1);
    std::vector<int> displacements(world_size);
    for (int i = 0, n = 0; i < displacements.size(); ++i, n += subVectorSizeMax, srcSize -= subVectorSizeMax) {
        displacements[i] = n;
        if (srcSize > 0) {
            if (srcSize - subVectorSizeMax < 0)
                sendCounts[i] = srcSize;
            else
                sendCounts[i] = subVectorSizeMax;
        } else sendCounts[i] = 0;
    }

    dest.resize(sendCounts[world_rank]);

    MPI_Scatterv(src.data(), sendCounts.data(), displacements.data(), datatype, dest.data(), sendCounts[world_rank],
                 datatype, 0, MPI_COMM_WORLD);

    return {sendCounts, displacements};
}

void printVector(const std::vector<int> &vec, const std::string &name) {
    std::cout << name << ": ";
    for (int i = 0; i < vec.size(); ++i) {
        std::cout << vec[i] << " \n"[i == vec.size()-1];
    }
}