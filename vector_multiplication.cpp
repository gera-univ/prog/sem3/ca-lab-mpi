#include <iostream>
#include <mpi.h>
#include <sstream>
#include <random>
#include <functional>
#include "common.h"

std::vector<int> getVectorOfRandomNumbers(int vectorSize, int from, int to);

int main(int argc, char *argv[]) {
    MPI_Init(nullptr, nullptr);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    std::vector<int> vector1;
    std::vector<int> vector2;

    int vectorSize;
    if (argc != 2)
        MPI_Abort(MPI_COMM_WORLD, 1);
    std::istringstream ss(argv[1]);
    ss >> vectorSize;

    if (world_rank == 0) {

        vector1 = getVectorOfRandomNumbers(vectorSize, -10, 10);
        vector2 = getVectorOfRandomNumbers(vectorSize, -10, 10);

        std::cout << std::showpos;
        printVector(vector1, "vector1");
        printVector(vector2, "vector2");
    }

    std::vector<int> subVector1;
    std::vector<int> subVector2;
    auto [sendCounts, displacements] = scatterVector(vector1, vectorSize, subVector1, MPI_INT, world_size, world_rank);
    scatterVector(vector2, vectorSize, subVector2, MPI_INT, world_size, world_rank);

    int result = std::inner_product(subVector1.begin(), subVector1.end(), subVector2.begin(), 0);

    std::vector<int> resultsVector;

    if (world_rank == 0) {
        resultsVector.resize(world_size);
    }

    MPI_Gather(&result, 1, MPI_INT, resultsVector.data(), 1, MPI_INT, 0, MPI_COMM_WORLD);

    if (world_rank == 0) {
        printVector(displacements, "displs");
        printVector(sendCounts, "counts");

        result = std::accumulate(std::begin(resultsVector), std::end(resultsVector), 0);

        std::cout << "the dot product of two vectors is " << result << "\n";
    }

    MPI_Finalize();
    return 0;
}

std::vector<int> getVectorOfRandomNumbers(int vectorSize, int from, int to) {
    std::vector<int> numbers(vectorSize);

    std::random_device rd;
    std::mt19937 re(clock());
    std::uniform_int_distribution ud(from, to);
    auto genNumber = [&ud, &re] { return ud(re); };
    std::generate(numbers.begin(), numbers.end(), genNumber);

    return numbers;
}
